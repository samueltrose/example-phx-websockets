# ApiMiddleware

The purpose of the application is to allow clients that connect across websockets a "middleware" that can do request to more traditional HTTP REST API services, validate responses, and send the response back to the websocket client.

This project contains an example front end (unde the fe-vanillajs folder), and an example websockets phoenix channels server.

The phoenix channels server is programmed to look for and consume an API spec from an upstream HTTP REST server, and then receive messages
on the phoenix websocket channels, validate them against the spec, and if valid, make the call to the upstream API, and return the response. 

This project has been used in real production deployments in the past. Currently the example only sends the healthcheck "PING" and resulting "PONG" message.

###To set up and test


To start your Api Middleware server:

  * Setup the project with `mix setup`
  * Start Phoenix endpoint with `mix phx.server`

The app will serve on  [`localhost:4002`](http://localhost:4002) it has no UI.

 
 Navigate into /fe-vanillajs and run `docker-compose up`, open developer browser web console in Chrome or Firefox, and navigate to [`localhost:8089`](http://localhost:8089). This page load is demonstrating the retrieval of service health check "PING" and resulting "PONG". Messages can be viewed by opening the developer tools console in the browser of the running application.