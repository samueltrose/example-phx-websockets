defmodule ApiMiddlewareWeb.ServiceChannel do
  use Phoenix.Channel

  def join("service:content", _message, socket) do
    {:ok, socket}
  end

  def join("service:" <> _message, _socket) do
    {:error, %{reason: "unauthorized"}}
  end

  def handle_in("msg", %{"body" => body}, socket) do
    if body == "PING" do
      push(socket, "msg", %{body: "PONG"})
      {:noreply, socket}
    else
      cond do
        path_validate(body) == false ->
          push(socket, "msg", %{
            body: "path #{body["path"]} or method *#{body["http_method"]}* not found"
          })

          {:noreply, socket}

        path_validate(body) == true ->
          resp = message_parse_and_request(body)
          response(socket, resp, body)
      end
    end
  end

  def response(socket, resp, body) do
    case resp do
      {:ok, resp} ->
        cond do
          resp.status_code == 200 ->
            respbody = Jason.decode!(resp.body)
            jsonbody = Jason.encode!(respbody)
            push(socket, "msg", %{body: jsonbody})
            {:noreply, socket}

          resp.status_code == 201 ->
            respbody = Jason.decode!(resp.body)
            jsonbody = Jason.encode!(respbody)
            push(socket, "msg", %{body: jsonbody})
            {:noreply, socket}

          resp.status_code == 400 ->
            push(socket, "msg", %{body: "you sent a bad or malformed request"})
            {:noreply, socket}

          resp.status_code == 401 ->
            push(socket, "msg", %{body: "Request unauthorized"})
            {:noreply, socket}

          resp.status_code == 404 ->
            push(socket, "msg", %{body: "path #{body["path"]} not found"})
            {:noreply, socket}

          resp.status_code == 500 ->
            push(socket, "msg", %{body: "the service responded with a server failure error"})
            {:noreply, socket}

          resp.status_code == 502 ->
            push(socket, "msg", %{body: "the service responded with a bad gateway"})
            {:noreply, socket}
        end

      {:error, resp} ->
        push(socket, "msg", %{body: "An error was received #{resp}"})
        {:noreply, socket}
    end
  end

  def message_parse_and_request(body) do
    method = http_method(body["http_method"])
    path = path_rewrite(body["path"], body)
    headers = [{"api_key", System.get_env("API_KEY")}]
    api_url = System.get_env("API_URL")
    Mojito.request(method, "#{api_url}#{path}", headers)
  end

  def http_method(http_method) do
    verbs = %{
      "connect" => :connect,
      "get" => :get,
      "head" => :head,
      "patch" => :patch,
      "post" => :post,
      "put" => :put,
      "options" => :options,
      "trace" => :trace
    }

    Map.get(verbs, http_method)
  end

  def path_validate(body) do
    pid = GenServer.whereis(ApiRoute)
    routes = GenServer.call(pid, :routes)

    Enum.any?(routes, fn x ->
      x.path == body["path"] and x.http_method == body["http_method"]
    end)
  end

  def path_rewrite(path, body) do
    path_parse(path, body)
    |> Path.join()
  end

  def path_parse(path, body) do
    pathlist = Path.split(path)

    Enum.map(pathlist, fn x ->
      match_path(x, body)
    end)
  end

  def match_path(fragment, body) do
    Map.get(body, fragment)
    |> path_flip(fragment)
  end

  def path_flip(result, fragment) do
    case result do
      nil ->
        fragment

      _ ->
        result
    end
  end
end
