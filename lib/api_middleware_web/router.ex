defmodule ApiMiddlewareWeb.Router do
  use ApiMiddlewareWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ApiMiddlewareWeb do
    pipe_through :api
  end
end
