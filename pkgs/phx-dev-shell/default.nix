{
  pkgs,
  stdenv,
  lib,
  mkShell,
}:
with pkgs;
  mkShell {
    buildInputs = [erlangR25 elixir_1_14 entr hivemind inotify-tools];
    shellHook = ''
      source $PWD/.env
      # Generic shell variables
      # This creates mix variables and data folders within your project, so as not to pollute your system
      mkdir -p .nix-mix
      mkdir -p .nix-hex
      export MIX_HOME=$PWD/.nix-mix
      export HEX_HOME=$PWD/.nix-hex
      export PATH=$MIX_HOME/bin:$PATH
      export PATH=$HEX_HOME/bin:$PATH
      echo 'To run the services configured here, you can run the `hivemind` command'
    '';
  }
