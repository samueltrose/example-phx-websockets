import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :api_middleware, ApiMiddlewareWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "femn995DemnfjQaCedtzIwE3S07VblCIEK+Xp+DMbH8VC92eN2nOoi5jPLx6jarv",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
